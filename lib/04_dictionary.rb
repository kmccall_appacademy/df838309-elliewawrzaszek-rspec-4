class Dictionary

  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(String)
      @entries[entry] = nil
    elsif entry.is_a?(Hash)
      @entries.merge!(entry)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    @entries.key?(keyword)
  end

  def find(term)
    @entries.select{|k, _| k.match(term)}
  end

  def printable
    print_version = []
    @entries.each do |key, _|
      print_version << %Q{[#{key}] "#{@entries[key]}"}
    end
    print_version.sort!.join("\n")
  end

end
