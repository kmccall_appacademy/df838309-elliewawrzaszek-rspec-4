class Temperature

  #  attr_accessor :fahrenheit, :celsius
  # # Hints
  #
  # Remember that one degree fahrenheit is 5/9 of one degree celsius,
  # and that the freezing point of water is 0 degrees celsius but 32
  # degrees fahrenheit.
  #
  # Remember to define the `from_celsius` factory method as a *class*
  # method, not an *instance* method.
  #
  # The temperature object's constructor should accept an *options hash*
  # which contains either a `:celcius` entry or a `:fahrenheit` entry.

  def initialize(options)
    if options[:f]
      self.fahrenheit = options[:f]
    else
      self.celsius = options[:c]
    end
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.ftoc(temp)
    (temp - 32) * 5/9
  end

  def self.ctof(temp)
    temp.to_f * 9.0/5.0 + 32.0
  end

  def fahrenheit=(temp)
    @temperature = self.class.ftoc(temp)
  end

  def celsius=(temp)
    @temperature = temp
  end

  def in_fahrenheit
      self.class.ctof(@temperature)
  end

  def in_celsius
    @temperature
  end

end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
