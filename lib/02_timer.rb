class Timer

  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def padded(number)
    num_str = number.to_s
    if num_str.length == 2
      num_str
    else
      until num_str.length == 2
        num_str += "0"
      end
      num_str.reverse
    end
  end

  def time_string
    if seconds < 60
      "00:00:" + padded(seconds)
    elsif seconds >= 60 && seconds < 3600
       minutes = seconds/60
       sec = seconds - (minutes * 60)
       "00:" + padded(minutes) + ":" + padded(sec)
    else
      hours = seconds / 3600
      minutes = ((seconds % 3600) / 60)
      sec = seconds % 60
      padded(hours) + ":" + padded(minutes) + ":" + padded(sec)
    end
  end

end
