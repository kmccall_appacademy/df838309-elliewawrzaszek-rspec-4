class Book
   attr_accessor :title

  def initialize
    @title = title
  end

  def title=(book_title)
    small_words = ["a", "an", "the", "in", "of", "and"]
    @title = book_title.split.map do |word|
      word.capitalize! unless small_words.include?(word)
      word

    end

    @title[0].capitalize!
    @title = @title.join(" ")
    
  end

end
